const exec = require('child_process').exec;
const randomString = require('randomstring');
const fs = require('fs');
var Jimp = require('jimp');
const { createWorker } = require('tesseract.js');

const BTN_INPUT = { x: 588, y: 1864 };
const BTN_NEXT = { x: 900, y: 1505 };
const BTN_CREATE = "Create account"
const ENTER_KEY = '_ENTER_'
const BTN_CREATE_ACCOUNT = { x: 228, y: 1619 };



// const
// 	io = require("socket.io"),
// 	server = io.listen(8000);

// let
// 	sequenceNumberByClient = new Map();

// server.on("connection", (socket) => {
// 	console.info(`Client connected [id=${socket.id}]`);
// 	sequenceNumberByClient.set(socket, 1);

// 	// when socket disconnects, remove it from the list:
// 	socket.on("disconnect", () => {
// 		sequenceNumberByClient.delete(socket);
// 		console.info(`Client gone [id=${socket.id}]`);
// 	});
// });

// setInterval(() => {
// 	for (const [client, sequenceNumber] of sequenceNumberByClient.entries()) {
// 		client.emit("input", 'tuan phan');
// 	}
// }, 1000);


function executeCommand(cmd) {
	return new Promise(function (resole, reject) {
		exec(cmd, function (err, stdOut, stdErr) {
			if (err) return reject(stdErr);
			return resole(stdOut);
		})
	})
}

function setInputValue(name, value) {
	fs.writeFileSync('value/' + name, value);
}

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}


async function getDeviceScreenShot(device) {
	try {
		var randomName = randomString.generate(5) + '.png';

		await executeCommand('.\\adb\\adb.exe -s ' + device + ' shell screencap /mnt/sdcard/Download/' + randomName + ' && .\\adb\\adb.exe -s ' + device + ' pull /mnt/sdcard/Download/' + randomName + ' tmp/' + randomName + '&& .\\adb\\adb.exe -s ' + device + ' shell rm /mnt/sdcard/Download/' + randomName)
		return 'tmp/' + randomName;
	} catch (err) {
		console.log(err)
	}
}

async function getDeviceList() {
	try {
		var deviceList = await executeCommand('.\\adb\\adb.exe devices');
		var lines = deviceList.split('\n')
		var deviceList = [];
		for (var i = 1; i < lines.length; i++) {
			if (lines[i].trim().length)
				deviceList.push(lines[i].split('\t')[0])
		}
		return deviceList;
	} catch (err) {
		return [];
	}
}

async function startAddAcountActivity(deviceId) {
	try {
		await executeCommand('.\\adb\\adb.exe -s ' + deviceId + ' shell am start -n com.example.addgoogleaccount/.MainActivity')

	} catch{


	}
}

async function changeAndroidId(deviceId) {
	try {
		await executeCommand('.\\adb\\adb.exe -s ' + deviceId + ' shell "su -c \'resetprop ro.boot.serialno ' + randomAid() + '\'"');
		await executeCommand('.\\adb\\adb.exe -s ' + deviceId + ' shell "su -c \'resetprop ro.serialno ' + randomAid() + '\'"');

		await executeCommand('.\\adb\\adb.exe -s ' + deviceId + ' shell "su -c \'content delete --uri content://settings/secure --where "name=\'android_id\'"\'"');
		await executeCommand('.\\adb\\adb.exe -s ' + deviceId + ' shell "su -c \'content insert --uri content://settings/secure --bind name:s:android_id --bind value:s:' + randomAid() + '\'"');


	} catch (err) {
		console.log(err)

	}
}
function randomAid() {
	var result = '';
	var characters = 'abcdef0123456789';
	var charactersLength = characters.length;
	for (var i = 0; i < 16; i++) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
}

async function cropImage(img) {
	var image = await Jimp.read(img);
	await image.crop(0, 100, 980, 1600);
	await image.writeAsync(img);
	return image;


}

function compareImages(image1, image2) {
	return new Promise(function (resolve, reject) {
		imageDiff({
			actualImage: image1,
			expectedImage: image2
		}, function (err, imagesAreSame) {
			if (err) return reject(err);
			return resolve(imagesAreSame);
		})
	})
}

async function waitForText(device, text, timeout, characterOnly) {

	var i = 0;
	while (i <= timeout) {
		var filePath = await getDeviceScreenShot(device);
		var coordinate = await getTextCoordinate(filePath, text, characterOnly);
		fs.unlinkSync(filePath)
		if (coordinate) return coordinate;
		await sleep(1000);
		i += 1000;
	}
	throw new Error('time out');

}


async function sendTouch(device, x, y) {
	await executeCommand('.\\adb\\adb.exe -s ' + device + ' shell "su -c \'input tap ' + x + ' ' + y + '\'"');

}

async function sendTextInput(device, text) {
	await executeCommand('.\\adb\\adb.exe -s ' + device + ' shell "su -c \'input text ' + text + '\'"');

}

async function sendKeyEvent(device, keyCode) {
	await executeCommand('.\\adb\\adb.exe -s ' + device + ' shell "su -c \'input keyevent ' + keyCode + '\'"');

}

async function connectADB(ipAddress) {
	await executeCommand('.\\adb\\adb.exe connect ' + ipAddress);

}


async function getTextCoordinate(filePath, text, characterOnly) {
	var worker = createWorker();
	await worker.load();
	await worker.loadLanguage('eng');
	await worker.initialize('eng');
	var data = await worker.recognize(filePath);
	await worker.terminate();
	for (var line of data.data.lines) {
		var tmp = line.text.trim()
		if (characterOnly) {
			tmp = line.text.replace(/[^0-9a-z ]/gi, '').trim();
		}
		if (tmp == text) {
			return { x: (line.bbox.x1 + line.bbox.x0) / 2, y: (line.bbox.y1 + line.bbox.y0) / 2 };
		}
	}
}

async function registerGmail(device, firstname, lastname, dob, mob, yob, username, pwd, recoveryEmail) {
	await changeAndroidId(device);
	await startAddAcountActivity(device);
	await sleep(7000);
	
	await sendTouch(device, BTN_CREATE_ACCOUNT.x, BTN_CREATE_ACCOUNT.y)
	await sleep(2000);
	await sendTouch(device, BTN_CREATE_ACCOUNT.x, BTN_CREATE_ACCOUNT.y)
	await sleep(2000);

	await sendTextInput(device, firstname);
	await sendKeyEvent(device, 66);
	await sleep(5000);




	await sendTextInput(device, firstname);


	await sendTouch(device, BTN_NEXT.x, BTN_NEXT.y);






}


async function run() {

	var deviceList = await getDeviceList();
	for (var device of deviceList) {
		await registerGmail(device, 'Tuan', 'Nguyen')
	}



}
run()



async function returnValue(req, res) {
	var id = req.params.id;
	try {
		var data = fs.readFileSync('value/' + id, 'utf8');

	} catch (error) {
		var data = "";
	}
	return res.send(data);

}

module.exports = {
	returnValue
};
