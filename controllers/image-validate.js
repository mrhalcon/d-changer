function pixelMatches(img1, img2, k, m, maxDelta, options, yOnly) {

	if ((k + 3) >= img1.length) {
		throw new Error(`Cannot get positions ${k} through ${k + 3} from img array of length ${img1.length}`);
	}
	if ((m + 3) >= img2.length) {
		throw new Error(`Cannot get positions ${m} through ${m + 3} from img array of length ${img2.length}`);
	}

	let r1 = img1[k + 0];
	let g1 = img1[k + 1];
	let b1 = img1[k + 2];
	let a1 = img1[k + 3];

	let r2 = img2[m + 0];
	let g2 = img2[m + 1];
	let b2 = img2[m + 2];
	let a2 = img2[m + 3];

	if (a1 === a2 && r1 === r2 && g1 === g2 && b1 === b2) return true;

	if (a1 < 255) {
		a1 /= 255;
		r1 = blend(r1, a1);
		g1 = blend(g1, a1);
		b1 = blend(b1, a1);
	}

	if (a2 < 255) {
		a2 /= 255;
		r2 = blend(r2, a2);
		g2 = blend(g2, a2);
		b2 = blend(b2, a2);
	}

	const y = rgb2y(r1, g1, b1) - rgb2y(r2, g2, b2);

	let delta;

	if (yOnly) { // brightness difference only
		delta = y;
	} else {
		const i = rgb2i(r1, g1, b1) - rgb2i(r2, g2, b2);
		const q = rgb2q(r1, g1, b1) - rgb2q(r2, g2, b2);
		delta = 0.5053 * y * y + 0.299 * i * i + 0.1957 * q * q;
	}
	return delta <= maxDelta;
}

function rgb2y(r, g, b) { return r * 0.29889531 + g * 0.58662247 + b * 0.11448223; }
function rgb2i(r, g, b) { return r * 0.59597799 - g * 0.27417610 - b * 0.32180189; }
function rgb2q(r, g, b) { return r * 0.21147017 - g * 0.52261711 + b * 0.31114694; }

// blend semi-transparent color with white
function blend(c, a) {
	return 255 + (c - 255) * a;
}


const defaultOptions = {
	threshold: 0.1,         // matching threshold (0 to 1); smaller is more sensitive
	// includeAA: false,       // whether to skip anti-aliasing detection. WIP (see pixelmatch package)
};

function subImageMatch(img, subImg, optionsParam) {

	const { data: imgData, width: imgWidth, height: imgHeight } = img;
	const { data: subImgData, width: subImgWidth } = subImg;

	if (!isPixelData(imgData) || !isPixelData(subImgData)) {
		throw new Error("Image data: Uint8Array, Uint8ClampedArray or Buffer expected.");
	}
	if (img.length < subImg.length) {
		throw new Error("Subimage is larger than base image");
	}
	const options = Object.assign({}, defaultOptions, optionsParam);
	const maxDelta = 35215 * options.threshold * options.threshold;

	let subImgPos = 0;
	let matchingTopRowStartX = 0;
	let matchingTopRowStartY = 0;

	for (let y = 0; y < imgHeight; y++) {

		matchingTopRowX = 0; // restart finding top row mode when we hit a new row in the main img
		for (let x = 0; x < imgWidth; x++) {

			const imgPos = posFromCoordinates(y, x, imgWidth);

			const matches = pixelMatches(imgData, subImgData, imgPos, subImgPos, maxDelta);
			if (matches) {

				if (matchingTopRowX === 0) {
					// This means this is a new matching row, save these coordinates in the matchingTopRowStartX and Y
					matchingTopRowStartX = x;
					matchingTopRowStartY = y;
				}

				matchingTopRowX++;
				if (matchingTopRowX === subImgWidth) {
					if (subImageMatchOnCoordinates(img, subImg, matchingTopRowStartY, matchingTopRowStartX, maxDelta)) {
						return true;
					}
					x = matchingTopRowStartX; // put our search position x back to where the matching row began
					matchingTopRowX = 0;
				}
			} else {
				matchingTopRowX = 0; // restart finding top row mode when 2 pixels don't match
			}
		}
	}
	return false;
}

function subImageMatchOnCoordinates(img, subImg, matchY, matchX, maxDelta) {
	const { data: imgData, width: imgWidth } = img;
	const { data: subImgData, width: subImgWidth, height: subImgHeight } = subImg;
	let subImgX = 0;
	let subImgY = 0;
	for (let imgY = matchY; imgY < (matchY + subImgHeight); imgY++) {
		subImgX = 0;

		for (let imgX = matchX; imgX < (matchX + subImgWidth); imgX++) {

			const imgPos = posFromCoordinates(imgY, imgX, imgWidth);
			const subImgPos = posFromCoordinates(subImgY, subImgX, subImgWidth);
			const matches = pixelMatches(imgData, subImgData, imgPos, subImgPos, maxDelta, undefined, imgY === 5);
			if (!matches) {
				return false;
			}
			subImgX++;
		}
		subImgY++;
	}
	return true;
}

function isPixelData(arr) {
	// work around instanceof Uint8Array not working properly in some Jest environments
	return ArrayBuffer.isView(arr) && arr.constructor.BYTES_PER_ELEMENT === 1;
}

function posFromCoordinates(y, x, width) {
	return (y * width + x) * 4;
}

module.exports = subImageMatch;
