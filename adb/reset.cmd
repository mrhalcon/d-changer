@echo off
cls
call :intToHex hex0 "(%random%<<15)+%random%"
call :intToHex hex1 "(%random%<<15)+%random%"
set "MAC=00:%hex1:~-4,2%:%hex1:~-2,2%:%hex0:~-6,2%:%hex0:~-4,2%:%hex0:~-2,2%"
echo %MAC%
setlocal enabledelayedexpansion
set "string=ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
set "SERIAL="
for /L %%i in (1,1,11) do call :add
echo %SERIAL%

setlocal enabledelayedexpansion

.\adb.exe shell "su -c 'echo %SERIAL% > /efs/FactoryApp/serial_no'"
.\adb.exe shell "su -c 'echo %MAC% > /efs/wifi/.mac.cob'"
.\adb.exe shell "su -c 'echo %MAC% > /efs/wifi/.mac.info'"

.\adb.exe shell "su -c 'pm clear com.google.android.gms'"
.\adb.exe shell "su -c 'pm clear com.google.android.gsf'"
.\adb.exe shell "su -c 'pm clear com.google.android.gsf.login'"
.\adb.exe shell "su -c 'pm clear com.google.android.googlequicksearchbox'"
.\adb.exe shell "su -c 'pm clear com.google.android.backuptransport'"
.\adb.exe shell "su -c 'pm clear com.google.android.partnersetup'"
.\adb.exe shell "su -c 'pm clear com.android.vending'"
.\adb.exe shell "su -c 'rm /data/system_ce/0/accounts_ce.db'"
.\adb.exe shell "su -c 'rm /data/system_de/0/accounts_de.db'"
.\adb.exe shell "su -c 'reboot'"


goto :eof

:intToHex
   setlocal enableDelayedExpansion
   set /A "num=%~2"
   set "hex="
   set "digits=0123456789ABCDEF"
   for /L %%a in (0,1,7) do (
      set /A "nibble=num&0xF", "num>>=4"
      for %%b in (!nibble!) do set "hex=!digits:~%%b,1!!hex!"
   )
   endlocal & set "%~1=%hex%"
   goto :eof
:add
	set /a x=%random% %% 36 
	set SERIAL=%SERIAL%!string:~%x%,1!
	goto :eof
 
 
